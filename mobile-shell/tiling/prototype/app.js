var animOptions, appDrawer, appdrawerBg, appdrawerCarousel, appdrawerHeight, arrow, bg, container, currentOverviewState, currentShellState, currentWorkspace, darken, fast, faster, h, medium, panel, panelHeight, slow, slower, systemStatus, time, w, windowCarousel, windowGap, windowHeight, windows;

w = 1280;

h = 768;

appdrawerHeight = 530;

windowHeight = 728;

windowGap = 50;

panelHeight = 40;

currentOverviewState = "SESSION";

currentShellState = "SESSION";

currentWorkspace = 1;

animOptions = {
  time: 0.2,
  curve: Spring({
    damping: 0.95
  })
};

slower = {
  time: 0.34,
  curve: Spring({
    damping: 0.86
  })
};

slow = {
  time: 0.28,
  curve: Spring({
    damping: 0.9
  })
};

medium = {
  time: 0.2,
  curve: Spring({
    damping: 0.97
  })
};

fast = {
  time: 0.12,
  curve: Spring({
    damping: 0.97
  })
};

faster = {
  time: 0.04,
  curve: Spring({
    damping: 0.99
  })
};

bg = new Layer({
  width: w,
  height: h,
  image: "assets/bg+panels.png",
  shadow1: {
    y: 8,
    blur: 20
  }
});

container = new Layer({
  parent: bg,
  width: w,
  height: h,
  x: 0,
  y: Align.center,
  scale: 1,
  image: "assets/wallpaper.png",
  animationOptions: animOptions
});

container.states.time = {
  scale: 0.9,
  x: 310
};

container.states.system = {
  scale: 0.9,
  x: -330
};

windowCarousel = new PageComponent({
  parent: container,
  width: w,
  height: h,
  y: Align.top,
  scrollVertical: false,
  scrollHorizontal: false,
  animationOptions: animOptions,
  ignoreEvents: true
});

windows = new Layer({
  parent: windowCarousel.content,
  width: w * 3 + windowGap * 2,
  height: windowHeight,
  scale: 1,
  x: -w - windowGap,
  y: Align.top,
  animationOptions: slow,
  image: "assets/windows.png"
});

windows.states.overview = {
  scale: 0.28,
  x: -w - windowGap,
  y: -250
};

windows.states.window2 = {
  scale: 1,
  x: -w - windowGap - (w + windowGap),
  y: Align.top
};

windows.states.window2overview = {
  scale: 0.28,
  x: -w - windowGap - (w + windowGap) * 0.28,
  y: -250
};

appdrawerBg = new Layer({
  parent: container,
  width: w,
  height: 0.01,
  y: h,
  image: "assets/appdrawer-bg.png",
  animationOptions: fast
});

appdrawerBg.states.overview = {
  height: appdrawerHeight,
  y: h - appdrawerHeight
};

appdrawerCarousel = new PageComponent({
  parent: container,
  width: w,
  height: appdrawerHeight,
  y: Align.bottom,
  originY: 0,
  scrollHorizontal: false,
  animationOptions: medium,
  ignoreEvents: true
});

panel = new Layer({
  parent: appdrawerCarousel.content,
  width: w,
  height: appdrawerHeight,
  y: 0,
  image: "asssets/appdrawer-panel-empty.png",
  animationOptions: fast
});

time = new Layer({
  parent: panel,
  width: 100,
  height: panelHeight,
  x: 0,
  y: Align.bottom,
  image: "assets/appdrawer-time.png",
  animationOptions: fast
});

time.states.active = {
  image: "assets/appdrawer-time-active.png"
};

arrow = new Layer({
  parent: panel,
  width: 200,
  height: panelHeight,
  x: Align.center,
  y: Align.bottom,
  image: "assets/appdrawer-arrow.png",
  animationOptions: faster
});

arrow.states.overview = {
  image: "assets/appdrawer-arrow-down.png"
};

systemStatus = new Layer({
  parent: panel,
  width: 100,
  height: panelHeight,
  x: w - 100,
  y: Align.bottom,
  image: "assets/appdrawer-system.png",
  animationOptions: fast
});

systemStatus.states.active = {
  image: "assets/appdrawer-system-active.png"
};

appDrawer = new Layer({
  width: w,
  height: appdrawerHeight - panelHeight,
  image: "assets/appdrawer-rest.png"
});

appdrawerCarousel.addPage(appDrawer, "bottom");

darken = new Layer({
  parent: container,
  width: w,
  height: h,
  x: Align.center,
  y: Align.center,
  backgroundColor: "#241f31",
  scale: 1 / 60,
  ignoreEvents: true,
  opacity: 0,
  animationOptions: fast
});

darken.states.visible = {
  scale: 1,
  opacity: 0.3,
  ignoreEvents: false
};

time.onTap(function() {
  if (currentShellState === "SESSION") {
    currentShellState = "TIME";
    container.animate("time");
    time.animate("active");
    return darken.animate("visible");
  }
});

systemStatus.onTap(function() {
  if (currentShellState === "SESSION") {
    currentShellState = "SYSTEM";
    container.animate("system");
    systemStatus.animate("active");
    return darken.animate("visible");
  }
});

arrow.onTap(function() {
  if (currentOverviewState === "SESSION") {
    currentOverviewState = "OVERVIEW";
    appdrawerCarousel.snapToPage(appDrawer, true, medium);
    arrow.stateSwitch("overview");
    appdrawerBg.animate("overview");
    if (currentWorkspace === 1) {
      return windows.animate("overview");
    } else {
      return windows.animate("window2overview");
    }
  } else {
    currentOverviewState = "SESSION";
    appdrawerCarousel.snapToPage(panel, true, medium);
    arrow.stateSwitch("default");
    appdrawerBg.animate("default");
    if (currentWorkspace === 1) {
      return windows.animate("default");
    } else {
      return windows.animate("window2");
    }
  }
});

windows.onTap(function() {
  if (currentOverviewState === "OVERVIEW") {
    if (currentWorkspace === 1) {
      currentWorkspace = 2;
      return windows.animate("window2overview");
    } else {
      currentWorkspace = 1;
      return windows.animate("overview");
    }
  }
});

darken.onTap(function() {
  currentShellState = "SESSION";
  container.animate("default");
  darken.animate("default");
  time.animate("default");
  return systemStatus.animate("default");
});
